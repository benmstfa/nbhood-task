<?php

if (isset($_POST['get_random'])) {
    $number1 = rand(0, 11);
    $number2 = rand(0, 11);
    $number3 = rand(0, 11);

    $response = array(
        'number1' => $number1,
        'number2' => $number2,
        'number3' => $number3,
        'win_state' => checkWinState($number1, $number2, $number3)
    );

    print_r(json_encode($response));
}

function checkWinState($number1, $number2, $number3)
{

    if (($number1 == $number2) && ($number1 == $number3)) {
        return "big win";
    } else if (($number1 == $number2) != ($number1 == $number3) && (($number1 == $number2) == true || ($number1 == $number3) == true) || ($number2 == $number3) == true) {
        return "small win";
    } else {
        return "no win";
    }
}

?>