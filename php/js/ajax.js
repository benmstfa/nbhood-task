$('#getRandom').on('click', function () {
    $.ajax({
        method: "post",
        url: "/php/back-end/server.php",
        data: {
            get_random: "get_random"
        },
        success: function (data) {
            data = JSON.parse(data)
            inHtml(data);
        }
    });
});

function inHtml(data) {
    document.getElementById("number1").innerHTML    = data.number1;
    document.getElementById("number2").innerHTML    = data.number2;
    document.getElementById("number3").innerHTML    = data.number3;
    document.getElementById("wind_state").innerHTML = data.win_state;
}