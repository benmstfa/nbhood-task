from flask import Flask, jsonify, request, render_template
import random

app = Flask(__name__)


@app.route('/')
def home():
    return render_template("index.html")


@app.route('/random', methods=['post'])
def post():
    number1 = random.randrange(0, 11)
    number2 = random.randrange(0, 11)
    number3 = random.randrange(0, 11)

    return jsonify({
        "number1": number1,
        "number2": number2,
        "number3": number3,
        "win_state": checkWinState(number1, number2, number3)
    })

def checkWinState(number1, number2, number3):
    if number1 == number2 and number1 == number3:
        return "big win"
    elif (number1 == number2) != (number1 == number3) and (
            (number1 == number2) == True or (number1 == number3) == True or (number2 == number3) == True):
        return "small win"
    else:
        return "no win"


app.run(port=5000, debug=True)
